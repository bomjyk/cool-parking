﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Timers;

namespace CoolParking.BL.Services 
{
    public class ParkingService : IParkingService
    {
        private ITimerService _timer;
        private ITimerService _logTimer;
        private ILogService _log;
        public Stack<TransactionInfo> TransactionHistory { get; set; }
        public void Dispose()
        {
            Parking.GetParking().Vehicles.Clear();
            Parking.GetParking().Balance = Settings.InitialParkingBalance;
            _timer.Stop();
            _logTimer.Dispose();
            _timer.Dispose();
            _logTimer.Dispose();
        }

        public decimal GetBalance()
        {
            return Parking.GetParking().Balance;
        }

        public int GetCapacity()
        {
            return Parking.GetParking().Capacity;
        }

        public int GetFreePlaces()
        {
            Parking tempParking = Parking.GetParking();
            int busyPlaces = tempParking.Vehicles.Count;
            return (tempParking.Capacity - busyPlaces);
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.GetParking().Vehicles);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (this.GetFreePlaces() == 0)
            {
                throw new InvalidOperationException("No more free space for vehicle");
            }
            List<Vehicle> vehicles = Parking.GetParking().Vehicles;
            int vehicleIndex = vehicles.FindIndex( (veh) =>  veh.Id == vehicle.Id);
            if (vehicleIndex >= 0)
            {
                throw new ArgumentException("There are already vehicle with such id.");
            }
            Parking.GetParking().Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            List<Vehicle> vehicles = Parking.GetParking().Vehicles;
            int vehicleIndex = vehicles.FindIndex( (vehicle) =>  vehicle.Id == vehicleId);
            if (vehicleIndex < 0)
            {
                throw new ArgumentException(" There are no vehicle with such id.");
            }
            if (vehicles[vehicleIndex].Balance < 0)
            {
                throw new InvalidOperationException("This vehicle are in dept. We can't remove it.");
            }
            Parking.GetParking().Vehicles.RemoveAt(vehicleIndex);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            List<Vehicle> vehicles = Parking.GetParking().Vehicles;
            int vehicleIndex = vehicles.FindIndex( (vehicle) =>  vehicle.Id == vehicleId);
            if (vehicleIndex < 0)
            {
                throw new ArgumentException("There are no vehicles with such id.");
            }

            if (sum < 0)
            {
                throw new ArgumentException();
            }
            vehicles[vehicleIndex].Balance += sum;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return TransactionHistory.ToArray();
        }

        public string ReadFromLog()
        {
            return _log.Read();
        }

        public ParkingService(ITimerService timer, ITimerService logTimer, ILogService log)
        {
            TransactionHistory = new Stack<TransactionInfo>();
            _timer = timer;
            _timer.Elapsed += onWithdrawnTimerElapsed;
            _logTimer = logTimer;
            _logTimer.Elapsed += onLogTimerElapsed;
            _log = log;
            _timer.Start();
            _logTimer.Start();
        }

        private void onWithdrawnTimerElapsed(object sender, ElapsedEventArgs args)
        {
            List<Vehicle> vehicles = Parking.GetParking().Vehicles;
            foreach (var vehicle in vehicles)
            {
                decimal withdrawnMoney = 0;
                decimal tariffPrice = Settings.Tariff.GetValueOrDefault(vehicle.VehicleType);
                if (vehicle.Balance > tariffPrice)
                {
                    vehicle.Balance -= tariffPrice;
                    Parking.GetParking().Balance += tariffPrice;
                    withdrawnMoney = tariffPrice;
                }
                else if (vehicle.Balance > 0 && vehicle.Balance < tariffPrice)
                {
                    decimal parkingSum = vehicle.Balance;
                    tariffPrice -= vehicle.Balance;
                    vehicle.Balance -= vehicle.Balance;
                    vehicle.Balance -= tariffPrice * Settings.PenaltyCoefficient;
                    parkingSum += tariffPrice * Settings.PenaltyCoefficient;
                    Parking.GetParking().Balance += parkingSum;
                    withdrawnMoney = parkingSum;
                }
                else
                {
                    vehicle.Balance -= tariffPrice * Settings.PenaltyCoefficient;
                    Parking.GetParking().Balance += tariffPrice * Settings.PenaltyCoefficient;
                    withdrawnMoney = tariffPrice * Settings.PenaltyCoefficient;
                }
                TransactionHistory.Push(new TransactionInfo(vehicle.Id, withdrawnMoney));
            }
        }

        private void onLogTimerElapsed(object sender, ElapsedEventArgs args)
        {
            if (TransactionHistory.Count == 0)
            {
                _log.Write("No transactions was made yet.");
            }
            while (TransactionHistory.Count > 0)
            {
                _log.Write(TransactionHistory.Pop().ToString());
            }
        }
    }
}