﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic.CompilerServices;


namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private readonly VehicleType _vehicleType;
        private readonly string _id;

        public  string Id
        {
            get { return _id; }
        }

        public VehicleType VehicleType
        {
            get { return _vehicleType; }
        }

        public  decimal Balance {  get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (balance < 0)
            {
                throw new ArgumentException("Balance can't be negative number.");
            }

            Regex regexIdPattern = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$",
                RegexOptions.Compiled);
            if (!regexIdPattern.IsMatch(id))
            {
                throw new ArgumentException();
            }
            _id = id;
            Settings.UniqueNumbers.Add(_id);
            _vehicleType = vehicleType;
            Balance = balance;
        }
        public static string GenerateRandomRegistrationPlateNumber()
        {
            Random randomGenerator = new Random();
            string uniqueNumber = generateId(randomGenerator);
            while (Settings.UniqueNumbers.Contains(uniqueNumber))
            {
                uniqueNumber = generateId(randomGenerator);
            }
            Settings.UniqueNumbers.Add(uniqueNumber);
            return uniqueNumber;
        }

        private static string generateId(Random random)
        {
            char[] idCharacters = new char[10];
            // 2 - 4 - 2 [0...1, 3...6, 8...9]
            //65 - 90 [A-Z] ; 48-57 [0-9]
            for (int i = 0; i < idCharacters.Length; ++i)
            {
                if (i > 2 && i < 7)
                {
                    idCharacters[i] = (char)random.Next(48, 57);
                } 
                else if (i == 2 || i == 7)
                {
                    idCharacters[i] = '-';
                }
                else
                {
                    idCharacters[i] = (char) random.Next(65, 90);
                }
            }

            return new string(idCharacters);
        }
    }
}