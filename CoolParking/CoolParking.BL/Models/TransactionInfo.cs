﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public DateTime TimeStamp { get; set; }
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }

        public TransactionInfo(string vehicleId, decimal sum)
        {
            TimeStamp = DateTime.Now;
            VehicleId = vehicleId;
            Sum = sum;
        }

        public override string ToString()
        {
            return $"\"{TimeStamp.ToString()}\" - \"{VehicleId}\" = \"{Sum.ToString()}\"";
        }
    }
}