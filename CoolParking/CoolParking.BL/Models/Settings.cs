﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly decimal InitialParkingBalance = 0;
        public static readonly int ParkingCapacity = 10;
        public static readonly int WithdrawPeriod = 5;
        public static readonly int LoggingPeriod = 60;

        public static readonly Dictionary<VehicleType, decimal> Tariff = new Dictionary<VehicleType, decimal>()
        {
            {VehicleType.PassengerCar, 2},
            {VehicleType.Truck, 5},
            {VehicleType.Bus,(decimal) 3.5},
            {VehicleType.Motorcycle,1}
        };

        public static readonly decimal PenaltyCoefficient = (decimal) 2.5;
        public static List<string> UniqueNumbers = new List<string>();
    }
}